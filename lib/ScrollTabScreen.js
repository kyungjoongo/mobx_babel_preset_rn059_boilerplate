// @flow

import React, {Component} from 'react';
import {Container, Text, Card, Header, Body, Button, Title, CardItem} from 'native-base';
import GlobalStore from './GlobalStore';
import {observer} from 'mobx-react';
import {ActivityIndicator, ScrollView, View, Dimensions, StyleSheet} from 'react-native';
import ScrollableTabView, {ScrollableTabBar,} from 'react-native-scrollable-tab-view';
import axios from "axios";
import cheerio from "react-native-cheerio";
import {renderBanner} from "./Shared";
import {TabView, SceneMap} from 'react-native-tab-view';


export default observer(
    class ScrollTabScreen extends Component {
        static navigationOptions = {
            header: null,
        };

        constructor() {
            super();
            this.state = {
                isReady: false,
                loading: true,
                index: 0,
                routes: [
                    {key: 'first', title: 'First'},
                    {key: 'second', title: 'Second'},
                ],
            }
        }

        async componentWillMount() {

        }

        componentDidMount() {

            this.getCheerios();
        }

        getCheerios() {

            this.setState({
                loading: true,
            })
            let url = 'https://nonojapan.com/search/?query=%EC%83%9D%ED%99%9C';
            axios({
                method: 'get', url: url,
                headers: {'Content-Type': 'text/html;charset=EUC-KR'},
                timeout: 1000 * 10,
            }).then(response => {
                let _html = response.data;
                const $ = cheerio.load(_html)

                let arrayresults = [];

                let __result = $('.site-content').html();

                console.log("1231233123123123-->", __result);


                $('.site-content > .container >  .mdl-grid > .mdl-cell ').each(function () {
                    let text = $(this).find('.mdl-card__title-text').text();

                    //mdl-card__supporting-text
                    let text2 = [];

                    $(this).find('.mdl-card__supporting-text > .btn ').each(function () {
                        let itmeOne = $(this).text();
                        text2.push(itmeOne);
                    });

                    console.log("text-->", text);
                    arrayresults.push({
                        text: text,
                        text2Array: text2,
                    })

                })

                console.log('results', arrayresults);

                this.setState({
                    results: arrayresults,
                    loading: false,
                })


            }).catch(e => alert(e));
        }

        /*  renderTab0() {
              return (

              )
          }*/

        renderTab002() {
            return (
                <View>
                    <Text>
                        sdflksdlfksdlfk
                    </Text>
                    <Text>
                        sdflksdlfksdlfk
                    </Text>
                    <Text>
                        sdflksdlfksdlfk
                    </Text>
                    <Text>
                        sdflksdlfksdlfk
                    </Text>
                </View>
            )
        }

        FirstRoute = () => (
            <View style={{flex: 1}}>
                {this.state.loading && <ActivityIndicator/>}
                <ScrollView>
                    <Text>sdlkfsdlkflsd</Text>
                    <Text>sdlkfsdlkflsd</Text>
                    <Text>sdlkfsdlkflsd</Text>
                    <Text>sdlkfsdlkflsd</Text>
                </ScrollView>

            </View>
        );

        SecondRoute = () => (
            <View style={[styles.scene, {backgroundColor: '#673ab7'}]}/>
        );


        render() {
            return (
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        first: this.FirstRoute,
                        second: this.SecondRoute,
                    })}
                    onIndexChange={index => {
                        this.setState({index})
                    }}
                    initialLayout={{width: Dimensions.get('window').width}}
                />
            );
        }
    }
)


const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
});