// @flow

import React, {Component} from 'react';
import {Container, Text, Card, Header, Body, Button, Title, CardItem} from 'native-base';
import GlobalStore from './GlobalStore';
import {observer} from 'mobx-react';
import {ActivityIndicator, View} from 'react-native';


/**
 * CounterScreen2
 */
export default observer(
    class CounterScreen3 extends Component {
        constructor() {
            super();
            this.state = {
                isReady: false
            }
        }

        async componentWillMount() {
            this.setState({isReady: true});
        }

        render() {
            if (!this.state.isReady) {
                return;
            }
            return (
                <Container>
                    <View>
                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 30,
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center'
                        }}>
                            {GlobalStore.counter}
                        </Text>
                    </View>

                    <Text>
                        CounterScreen3CounterScreen3CounterScreen3

                    </Text>
                    <Text>
                        CounterScreen3
                    </Text>

                    {GlobalStore.loading && <ActivityIndicator color={'red'} size={'large'}/>}
                </Container>
            );
        }
    }
)
