import React, {Component} from 'react';
import {Container, Text, Header, Body, Title, Card, CardItem, CardSwiper} from 'native-base';
import GlobalStore from './GlobalStore';
import {observer} from 'mobx-react';
import {observable, decorate} from 'mobx';
import {View, ScrollView, TouchableOpacity, ToastAndroid, Image, StyleSheet, Platform} from 'react-native';
import {renderBanner, showAdmobInit} from "./Shared";
import {AdMobBanner} from "react-native-admob";
import axios from 'axios';
import cheerio from "react-native-cheerio";
import {ActivityIndicator, Linking} from 'react-native'
import FacebookTabBar from "./FacebookTabBar";
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';

export default observer(
    class MainScreen2 extends Component {
        static navigationOptions = {
            header: null,
        };

        constructor() {
            super();
            this.state = {
                isReady: false,
                results: [],
                cate: '전체',
                tabIndex: 0,
            }
        }

        async componentWillMount() {
            this.setState({isReady: true});
        }

        componentDidMount() {
            Linking.addEventListener('url', this._handleOpenURL);
            // showAdmobInit();
            let results = this.getCHeeiros();

            this.setState({
                results: results,
                isReady: true,
            })
           /* if (Platform.os === 'android') {
                ToastAndroid.showWithGravityAndOffset(
                    '상품명을 클릭하시면 상품 이미지를 보실 수 있습니다.^^',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                );
            }*/
        }

        componentWillUnmount() {
            Linking.removeEventListener('url', this._handleOpenURL);
        }

        _handleOpenURL(event) {
            showAdmobInit();
        }

        async getCHeeiros() {

            let url = '';
            if (this.state.cate === '전체') {
                url = 'https://nonojapan.com';
            } else {
                url = 'https://nonojapan.com/search/?query=' + encodeURI(this.state.cate);
            }


            this.setState({
                loading: true,
            })
            return await axios({
                method: 'get', url: url,
                headers: {'Content-Type': 'text/html;charset=EUC-KR'},
                timeout: 1000 * 10,
            }).then(response => {
                let _html = response.data;
                const $ = cheerio.load(_html)

                let arrayresults = [];

                let __result = $('.site-content').html();

                console.log("1231233123123123-->", __result);


                $('.site-content > .container >  .mdl-grid > .mdl-cell ').each(function () {
                    let text = $(this).find('.mdl-card__title-text').text();

                    //mdl-card__supporting-text
                    let text2 = []

                    let index = 0;
                    $(this).find('.mdl-card__supporting-text > .btn ').each(function () {
                        let itmeOne = $(this).text();
                        text2.push(itmeOne);
                        index++;
                    });

                    console.log("text-->", text);
                    arrayresults.push({
                        text: text,
                        text2Array: text2
                    })

                })

                console.log('results', arrayresults);

                return arrayresults;


            }).catch(e => alert(e));
        }

        changeTab(cate) {
            GlobalStore.incrementForTab();
            this.setState({
                cate: cate,
                results: [],
            }, () => {
                this.getCHeeiros();
            })
        }

        renderTopScrollView() {
            let backGroundColor1 = '#b9b9b9'
            let _height = 55;

            return (
                <ScrollView horizontal={true} style={{height: 50}}>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center",
                        }}
                        onPress={() => {

                            this.changeTab('전체')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>전체</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}
                        onPress={() => {

                            this.changeTab('생활')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>생활</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}

                        onPress={() => {
                            this.changeTab('음식')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>음식</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}

                        onPress={() => {
                            this.changeTab('가전')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>가전</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}

                        onPress={() => {
                            this.changeTab('화장품')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>화장품</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}

                        onPress={() => {
                            this.changeTab('의약품')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>의약품</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}

                        onPress={() => {
                            this.changeTab('패션')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>패션</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}

                        onPress={() => {
                            this.changeTab('취미')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>취미</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}

                        onPress={() => {
                            this.changeTab('자동차')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>자동차</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}

                        onPress={() => {
                            this.changeTab('금융')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>금융</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: 70,
                            backgroundColor: backGroundColor1,
                            alignSelf: 'center',
                            alignItem: 'center',
                            height: 50,
                            justifyContent: "center"
                        }}
                        onPress={() => {
                            this.changeTab('기타')
                        }}
                    >
                        <Text style={{textAlign: 'center'}}>기타</Text>
                    </TouchableOpacity>
                </ScrollView>
            )
        }

        renderContents() {

            if (!this.state.isReady) {
                return (
                    <View>

                    </View>
                )
            }

            if (this.state.tabIndex === 0) {
                return (

                    <Container style={{backgroundColor: '#e3e3e3'}}>
                        <Header style={{backgroundColor: '#4e0db4'}}>
                            <Body>
                            <Title style={{marginLeft: 10, marginBottom: 0}}>
                                {this.state.cate}
                            </Title>
                            </Body>
                        </Header>

                        <View style={{position: 'absolute', top: 50, height: 55}}>
                            {this.renderTopScrollView()}
                        </View>

                        {this.state.loading && <View style={{marginTop: 100}}>
                            <ActivityIndicator color={'red'}/>
                        </View>}

                        <Text>
                            sdlfksdlkfsdlkflskdflk
                        </Text>

                        <ScrollView style={{marginBottom: 55, marginTop: 55}}>
                            {this.state.results.map((item, index) => {

                                return (
                                    <Card>
                                        <CardItem header bordered>
                                            <TouchableOpacity
                                                style={{
                                                    backgroundColor: 'white',
                                                    height: 50,
                                                    alignItem: 'center',
                                                    alignSelf: 'center',
                                                    width: '100%',
                                                    //marginLeft: 30,
                                                    //borderRadius:50,
                                                    justifyContent: 'center',

                                                    flexDirection: 'row',

                                                }}
                                                onPress={() => {
                                                    showAdmobInit();
                                                    //Linking.openURL("https://www.google.com/search?q=" + encodeURI(item.text) + "&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjPh4Pd-MTjAhXlw4sBHbRmAYAQ_AUIESgB&biw=1445&bih=891").catch((err) => console.error('An error occurred', err));

                                                    let url = "https://www.google.com/search?q=" + encodeURI(item.text) + "&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjPh4Pd-MTjAhXlw4sBHbRmAYAQ_AUIESgB&biw=1445&bih=891"
                                                    this.props.navigation.push('WebViewScreen', {
                                                        url: url,
                                                    })
                                                }}
                                            >
                                                <View style={{flex: .16,}}>
                                                    <Image
                                                        style={{width: 50, height: 50}}
                                                        source={require('./jpn_flag.png')}
                                                    />
                                                </View>
                                                <View style={{
                                                    flex: .9,
                                                    alignItem: 'center',
                                                    justifyContent: 'center',
                                                    alignSelf: 'center',
                                                    marginLeft: 10,
                                                }}>
                                                    <Text style={{color: 'black', fontSize: 20}}>
                                                        {item.text}
                                                    </Text>
                                                </View>

                                            </TouchableOpacity>
                                        </CardItem>
                                        <CardItem footer bordered>
                                            <Text>대체품</Text>
                                        </CardItem>
                                        <CardItem bordered>
                                            <Body>
                                            <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>

                                                {item.text2Array.map((childItem, index) => {
                                                    return (
                                                        <TouchableOpacity
                                                            onPress={() => {
                                                                GlobalStore.increment();
                                                                let url = "https://www.google.com/search?q=" + encodeURI(childItem) + "&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjPh4Pd-MTjAhXlw4sBHbRmAYAQ_AUIESgB&biw=1445&bih=891"
                                                                this.props.navigation.push('WebViewScreen', {
                                                                    url: url,
                                                                })
                                                            }}
                                                        >
                                                            {index === item.text2Array.length - 1 ?
                                                                <Text
                                                                    style={{
                                                                        color: 'black',
                                                                        fontSize: 20,
                                                                        marginLeft: 2,
                                                                    }}>
                                                                    {childItem}
                                                                </Text>
                                                                :
                                                                <Text
                                                                    style={{
                                                                        color: 'black',
                                                                        fontSize: 20,
                                                                        marginLeft: 2,
                                                                    }}>
                                                                    {childItem},
                                                                </Text>
                                                            }


                                                        </TouchableOpacity>
                                                    )
                                                })}

                                            </View>

                                            </Body>
                                        </CardItem>


                                    </Card>
                                )
                            })}
                        </ScrollView>
                        <View style={{
                            alignSelf: 'center',
                            alignItem: 'center',
                            justifyContent: "center",
                            position: 'absolute',
                            bottom: 0
                        }}>
                            {renderBanner()}
                        </View>
                    </Container>
                )
            }

            if (this.state.tabIndex === 1) {
                return (
                    <View>
                        <Text>1111</Text>
                        <Text>1111</Text>
                        <Text>11111</Text>
                    </View>
                )
            }

            if (this.state.tabIndex === 2) {
                return (
                    <View>
                        <Text>22222</Text>
                        <Text>22222</Text>
                        <Text>22222</Text>
                    </View>
                )
            }


        }

        render() {
            if (!this.state.isReady) {
                return;
            }
            return (


                <ScrollableTabView
                    style={{marginTop: 20,}}
                    initialPage={0}
                    renderTabBar={() => <ScrollableTabBar/>}

                    onChangeTab={async (index) => {

                        console.log("sdlkfsdlkfds-->", index.from);
                        console.log("sdlkfsdlkfds-->", index.i);

                        let __result = await this.getCHeeiros();


                        this.setState(() => ({
                            tabIndex: index.i,
                            results: __result,
                        }))
                    }}
                >

                    <View tabLabel='11111111'>
                        <View style={{flex: 1}}>
                            {this.renderContents()}
                        </View>
                    </View>
                    <View tabLabel='Tab #2 word word'>
                        <View>
                            {this.renderContents()}
                        </View>
                    </View>
                    <View tabLabel='Tab #2 word word'>
                        <View>
                            {this.renderContents()}
                        </View>
                    </View>
                    <View tabLabel='Tab #2 word word'>
                        <View>
                            {this.renderContents()}
                        </View>
                    </View>
                    <View tabLabel='Tab #4 word word'>
                        <View>
                            {this.renderContents()}
                        </View>
                    </View>


                </ScrollableTabView>
            );
        }
    }
)

const styles = StyleSheet.create({
    tabView: {
        flex: 1,
        padding: 10,
        backgroundColor: 'rgba(0,0,0,0.01)',
    },
    card: {
        borderWidth: 1,
        backgroundColor: '#fff',
        borderColor: 'rgba(0,0,0,0.1)',
        margin: 5,
        height: 150,
        padding: 15,
        shadowColor: '#ccc',
        shadowOffset: {width: 2, height: 2,},
        shadowOpacity: 0.5,
        shadowRadius: 3,
    },
});
