import {observable} from 'mobx';
import {showAdmobInit} from "./Shared";


class GlobalStore {
    @observable counter = 0;
    @observable loading = false;
    @observable japanList = []
    @observable databaseList = []


    setDatabaseList ( arrList){
        this.databaseList=arrList;
    }

    setJapanList (arrList) {
        this.japanList = arrList;
    }

    increment() {
        this.counter++;
        console.log("increment", this.counter);
        if (this.counter % 5 ===0){
            showAdmobInit();
        }
    }

    increment3() {
        if(this.counter===0){
            showAdmobInit();
        }else{
            this.counter++;
            console.log("increment", this.counter);
            if (this.counter % 3 ===0){
                showAdmobInit();
            }
        }


    }

    incrementForTab() {
        this.counter++;
        console.log("increment", this.counter);
        if (this.counter % 10 ===0){
            showAdmobInit();
        }
    }

    decrement() {
        this.counter--;
        console.log("decrement", this.counter);
    }


    incrementDouble() {
        this.counter = this.counter + 2;
        console.log("increment2", this.counter);
    }

    toggleLoading(value: boolean = false) {

        this.loading = !this.loading
    }
}


export default new GlobalStore();