import React, {Component,} from "react";
import {Animated, Easing, ScrollView,} from "react-native";
import {createDrawerNavigator, createStackNavigator, DrawerItems, SafeAreaView,} from "react-navigation";
import MainScreen from "./lib/MainScreen";
import CounterScreen2 from "./lib/CounterScreen2";
import CounterScreen3 from "./lib/CounterScreen3";
import WebViewScreen from "./lib/WebViewScreen";
import SplashScreen from 'react-native-splash-screen'
const transitionConfig = {
    transitionSpec: {
        duration: 300,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing,
    },
    screenInterpolator: (sceneProps: any) => {
        const {layout, position, scene} = sceneProps;
        const {index} = scene;

        const height = layout.initHeight;
        const translateY = position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [height, 0, 0],
        });

        const opacity = position.interpolate({
            inputRange: [index - 1, index - 0.99, index],
            outputRange: [0, 1, 1],
        });

        return {opacity, transform: [{translateY}]};
    },
}

const CustomDrawerContentComponent = (props: any) => (
    <ScrollView style={{backgroundColor: '#121B34',}}>
        <SafeAreaView forceInset={{top: 'always', horizontal: 'never',}}>
            <DrawerItems {...props} />

        </SafeAreaView>
    </ScrollView>
);


const CounterStack = createStackNavigator({
    /* ScrollTabScreen:{
         screen:ScrollTabScreen
     },*/
    MainScreen: {
        screen: MainScreen
    },
    WebViewScreen: {
        screen: WebViewScreen,
    }

});


/**
 * #################################################
 * #################################################
 * _DrawerNavigator 부분
 * #################################################
 * #################################################
 */
const _DrawerNavigator = createDrawerNavigator({
    CounterStack: {
        screen: CounterStack,
        navigationOptions: {
            drawerLabel: 'Japan NONO',
        }
    },


}, {
    //####################################
    //이니셜 라우터(스크린) 설정부분
    //####################################
    initialRouteName: 'CounterStack',
    contentOptions: {
        // activeTintColor: '#2e7bff',
        activeTintColor: 'white',
        inactiveTintColor: 'gray',
    },
    drawerBackgroundColor: 'white',
    drawerPosition: 'left',
    drawerWidth: 200,
    contentComponent: CustomDrawerContentComponent,


});

export interface Props {
}

interface State {

}


export default class App extends Component<Props, State> {

    constructor(props: Props) {
        super(props);


    }


    componentWillUnmount() {

    }

    componentDidMount(){
        setTimeout(()=>{
            SplashScreen.hide();
        },1000)
    }


    render() {
        return (
            <CounterStack/>
        )

    }
}
