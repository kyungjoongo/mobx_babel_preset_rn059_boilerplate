// @flow

import React, {Component} from 'react';
import {Container, Text, Card, Header, Body, Button, Title, CardItem} from 'native-base';
import GlobalStore from './GlobalStore';
import {observer} from 'mobx-react';
import {View, ActivityIndicator, WebView} from 'react-native';


export default observer(
    class WebViewScreen extends Component {
        constructor() {
            super();
            this.state = {
                isReady: false,
                url: '',
            }
        }

        async componentWillMount() {
            let url = this.props.navigation.getParam('url');
           // alert(url)
            this.setState({
                url: url,
            })
        }

        componentDidMount() {

        }

        render() {
            return (
                <WebView
                    source={{uri: this.state.url}}
                    style={{marginTop: 0}}
                />
            );
        }
    }
)
