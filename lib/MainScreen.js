import React, {Component} from 'react';
import {Container, Text, Header, Body, Title, Card, CardItem, CardSwiper} from 'native-base';
import GlobalStore from './GlobalStore';
import {observer} from 'mobx-react';
import {observable, decorate} from 'mobx';
import {
    View,
    ScrollView,
    TouchableOpacity,
    ToastAndroid,
    Image,
    Platform,
    FlatList,
    TouchableWithoutFeedback
} from 'react-native';
import {renderBanner, showAdmobInit, showToast} from "./Shared";
import {AdMobBanner} from "react-native-admob";
import axios from 'axios';
import cheerio from "react-native-cheerio";
import {ActivityIndicator, Linking} from 'react-native'
import Toast from 'react-native-root-toast';
import {toJS} from 'mobx';
import {FlingGestureHandler, Directions, State,} from 'react-native-gesture-handler'


export default observer(
    class MainScreen extends Component {
        static navigationOptions = {
            header: null,
        };

        constructor() {
            super();

            this.state = {
                isReady: false,
                results: [],
                cate: '전체',
                currentTabIndex: 0,
                page: 1,
                page2: 1,
                isMoreData: true,
                isMoreData2: true,
                bottomLoading: false,
                myText: 'I\'m ready to get swiped!',
                gestureName: 'none',
                backgroundColor: '#fff'
            }
        }

        async componentWillMount() {
            this.setState({isReady: true});
        }

        async componentDidMount() {
            Linking.addEventListener('url', this._handleOpenURL);
            // showAdmobInit();
            this.getFirstDatas();
            //await this.makeDatabases();
            if (Platform.os === 'android') {
                ToastAndroid.showWithGravityAndOffset(
                    '상품명을 클릭하시면 상품 이미지를 보실 수 있습니다.^^',
                    ToastAndroid.SHORT,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                );
            } else {
                let toast = Toast.show('상품명을 클릭하시면 상품 이미지를 보실 수 있습니다.^^', {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.BOTTOM,
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                });
            }
        }

        async componentWillUnmount() {

            Linking.removeEventListener('url', this._handleOpenURL);
        }

        _handleOpenURL(event) {
            showAdmobInit();
        }

        /*async makeDatabases() {

            let url = 'https://nonojapan.com/products/' + this.state.page2;
            this.setState({
                loading: true,
            })
            let resultArray = await axios({
                method: 'get', url: url,
                headers: {'Content-Type': 'text/html;charset=EUC-KR'},
                timeout: 1000 * 10,
            }).then(response => {
                let resultArray = response.data.result.content;
                console.log("sdlkfsdlkfds-->", resultArray);
                return resultArray;

            });

            GlobalStore.setDatabaseList(resultArray)
            this.setState({
                loading: false,
                page: this.state.page2 + 1,
            }, async () => {
                while (this.state.isMoreData2) {
                    await this._______moreData2();
                }
                console.log("databaseList-->", toJS(GlobalStore.databaseList));
            })
        }

        async _______moreData2() {

            let url = 'https://nonojapan.com/products/' + this.state.page2;
            console.log("page-->", this.state.page2);

            this.setState(() => ({
                isMoreData2: false,
            }))
            let mergedArrays = await axios({
                method: 'get', url: url,
                headers: {'Content-Type': 'text/html;charset=EUC-KR'},
                timeout: 1000 * 10,
            }).then(response => {
                let moreResultArray = response.data.result.content;

                console.log("moreResultArray-->", moreResultArray);

                let preArrays = GlobalStore.databaseList

                let moreResultArrayLength = moreResultArray.length

                let mergedArrays = preArrays.concat(moreResultArray)

                this.setState(() => ({
                    isMoreData2: moreResultArrayLength >= 50,
                    page2: this.state.page2 + 1,
                }), () => {
                })

                return mergedArrays;


            })

            GlobalStore.setDatabaseList(mergedArrays);
            this.setState({
                page2: this.state.page2 + 1,
            })
        }*/


        getFirstDatas() {

            let url = '';
            if (this.state.cate === '전체') {
                url = 'https://nonojapan.com/products/' + this.state.page;
            } else {
                url = 'https://nonojapan.com/search/?query=' + encodeURI(this.state.cate);
            }

            let pushedList = [];
            let arrayresults = [];
            this.setState({
                loading: true,
            })
            axios({
                method: 'get', url: url,
                headers: {'Content-Type': 'text/html;charset=EUC-KR'},
                timeout: 1000 * 10,
            }).then(response => {
                let resultArray = response.data.result.content;

                console.log("sdlkfsdlkfds-->", resultArray);

                GlobalStore.setJapanList(resultArray);

                this.setState({
                    loading: false,
                    page: this.state.page + 1,

                })


            }).catch(e => alert('getFirstDatas' + e));
        }

        _getMoreDatas = () => {

            let url = '';
            if (this.state.cate === '전체') {
                url = 'https://nonojapan.com/products/' + this.state.page;
            } else {
                url = 'https://nonojapan.com/search/?query=' + encodeURI(this.state.cate);
            }

            console.log("page-->", this.state.page);

            this.setState({
                bottomLoading: true,
            })

            let pushedList = [];
            let arrayresults = [];
            this.setState(() => ({
                isMoreData: false,
            }))
            axios({
                method: 'get', url: url,
                headers: {'Content-Type': 'text/html;charset=EUC-KR'},
                timeout: 1000 * 10,
            }).then(response => {
                let moreResultArray = response.data.result.content;

                console.log("moreResultArray-->", moreResultArray);

                let preArrays = GlobalStore.japanList

                let moreResultArrayLength = moreResultArray.length


                let mergedArrays = preArrays.concat(moreResultArray)

                GlobalStore.setJapanList(mergedArrays);

                this.setState({
                    page: this.state.page + 1,
                    isMoreData: moreResultArrayLength >= 50,
                    bottomLoading: false,
                })


            }).catch(e => alert('_getMoreDatas' + e));
        }


        getCheeiros() {

            let url = '';
            if (this.state.cate === '전체') {
                url = 'https://nonojapan.com/products/1';
            } else {
                url = 'https://nonojapan.com/search/?query=' + encodeURI(this.state.cate);
            }


            this.setState({
                loading: true,
            })
            GlobalStore.setJapanList([]);
            axios({
                method: 'get', url: url,
                headers: {'Content-Type': 'text/html;charset=EUC-KR'},
                timeout: 1000 * 10,
            }).then(response => {
                let _html = response.data;
                const $ = cheerio.load(_html)

                let arrayresults = [];

                let __result = $('.site-content').html();

                console.log("1231233123123123-->", __result);


                let pushedList = [];
                $('.site-content > .container >  .mdl-grid > .mdl-cell ').each(function () {
                    let text = $(this).find('.mdl-card__title-text > h3').text();
                    let cate = $(this).find('.mdl-card__title-text > .pink-text').text();

                    //mdl-card__supporting-text
                    let childArray = []

                    let index = 0;
                    $(this).find('.mdl-card__supporting-text > .btn ').each(function () {
                        let itmeOne = $(this).text();
                        childArray.push(itmeOne);
                        index++;
                    });

                    let txtArray = text.toString().split(".");

                    console.log("text-->", txtArray[0]);

                    pushedList.push(text);

                    arrayresults.push({
                        name: text,
                        cate: cate,
                        altProducts: childArray
                    })

                })

                GlobalStore.setJapanList(arrayresults)

                console.log("pushedList-->", toJS(GlobalStore.japanList));

                console.log('results', arrayresults);


                this.setState({
                    loading: false,
                })


            }).catch(e => alert('cccccherih' + e));
        }

        changeTab(cate, index) {


            if (cate === '전체') {
                this.setState({
                    page: 1,
                    isMoreData: true,
                })
                GlobalStore.setJapanList([]);
                GlobalStore.incrementForTab();
                this.setState({
                    cate: cate,
                    currentTabIndex: index,
                    results: [],
                }, () => {
                    this.getFirstDatas();
                })
            } else {
                GlobalStore.incrementForTab();
                this.setState({
                    cate: cate,
                    currentTabIndex: index,
                    results: [],
                    isMoreData: false,
                }, () => {
                    this.getCheeiros();
                })
            }


        }

        cates = ['전체', '생활', '음식', '가전', '화장품', '의약품', '패션', '취미', '자동차', '금융', '반려동물', '기타']

        renderTabTab() {
            let backGroundColor1 = '#b9b9b9'
            let _height = 55;

            return (
                <ScrollView horizontal={true} style={{height: 50}} ref={c => this.topScrollView = c}

                >
                    {this.cates.map((item, index) => {
                        return (
                            <TouchableOpacity
                                key={index}
                                style={{
                                    width: 70,
                                    backgroundColor: this.state.currentTabIndex === index ? 'red' : 'grey',
                                    alignSelf: 'center',
                                    alignItem: 'center',
                                    height: 50,
                                    justifyContent: "center",

                                }}
                                onPress={() => {

                                    if (index >= 2 && index <= 5) {
                                        this.topScrollView.scrollTo({x: 130, y: 0, duration: 500})
                                    }else if (index >= 6 && index <= 9) {
                                        this.topScrollView.scrollTo({x: 230, y: 0, duration: 500})
                                    }/*else if (index >= 10 && index <= 13) {
                                        this.topScrollView.scrollTo({x: 330, y: 0, duration: 500})
                                    }*/


                                    /*else if ( index >5){
                                                                            this.topScrollView.scrollTo({x: 250, y: 0, duration: 500})
                                                                        }*/

                                    this.changeTab(item, index)
                                }}
                            >
                                <Text style={{textAlign: 'center'}}>{item}</Text>
                            </TouchableOpacity>
                        )
                    })}


                </ScrollView>
            )
        }

        _renderItem = ({item, index}) => {
            return (
                <Card>
                    <CardItem header bordered>
                        <TouchableOpacity
                            style={{
                                backgroundColor: 'white',
                                height: 50,
                                alignItem: 'center',
                                alignSelf: 'center',
                                width: '100%',
                                //marginLeft: 30,
                                //borderRadius:50,
                                justifyContent: 'center',

                                flex: .7,
                                flexDirection: 'row',

                            }}
                            onPress={() => {
                                GlobalStore.increment3();
                                //Linking.openURL("https://www.google.com/search?q=" + encodeURI(item.text) + "&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjPh4Pd-MTjAhXlw4sBHbRmAYAQ_AUIESgB&biw=1445&bih=891").catch((err) => console.error('An error occurred', err));

                                let url = "https://www.google.com/search?q=" + encodeURI(item.name) + "&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjPh4Pd-MTjAhXlw4sBHbRmAYAQ_AUIESgB&biw=1445&bih=891"
                                this.props.navigation.push('WebViewScreen', {
                                    url: url,
                                })
                            }}
                        >
                            <View style={{flex: .22,}}>
                                <Image
                                    style={{width: 50, height: 50}}
                                    source={require('./jpn_flag.png')}
                                />
                            </View>
                            <View style={{
                                flex: .7,
                                alignItem: 'center',
                                justifyContent: 'center',
                                alignSelf: 'center',
                                marginLeft: 30,
                            }}>
                                <Text style={{color: 'black', fontSize: 20}}>
                                    {item.name}
                                </Text>

                            </View>


                        </TouchableOpacity>
                        {/*<View style={{flex:.15, marginTop:-35, }}>
                                            <Text style={{textAlignVertical: 'center'}}>{item.cate}</Text>
                                        </View>*/}
                        <TouchableOpacity
                            style={{flex: .35, alignItem: 'center', justifyContent: 'center', alignSelf: 'center'}}
                            onPress={() => {
                                GlobalStore.increment3();
                                //Linking.openURL("https://www.google.com/search?q=" + encodeURI(item.text) + "&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjPh4Pd-MTjAhXlw4sBHbRmAYAQ_AUIESgB&biw=1445&bih=891").catch((err) => console.error('An error occurred', err));
                                let url = "https://ko.wikipedia.org/wiki/" + encodeURI(item.name);
                                this.props.navigation.push('WebViewScreen', {
                                    url: url,
                                })
                            }}
                        >
                            {/* <Image
                                                style={{width: 30, height: 30, alignItem:'center'}}
                                                source={require('./wiki.png')}
                                            />*/}
                            <Text style={{textAlign: 'center', color: 'green'}}>
                                Wiki
                            </Text>
                        </TouchableOpacity>

                    </CardItem>
                    <CardItem footer bordered>
                        <Text>대체품</Text>
                    </CardItem>
                    <CardItem bordered>
                        <Body>
                        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>

                            {item.altProducts.map((childItem, index) => {
                                return (
                                    <TouchableOpacity
                                        key={index}
                                        onPress={() => {
                                            GlobalStore.increment();
                                            let url = "https://www.google.com/search?q=" + encodeURI(childItem) + "&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjPh4Pd-MTjAhXlw4sBHbRmAYAQ_AUIESgB&biw=1445&bih=891"
                                            this.props.navigation.push('WebViewScreen', {
                                                url: url,
                                            })
                                        }}
                                    >
                                        {index === item.altProducts.length - 1 ?
                                            <Text
                                                style={{color: 'black', fontSize: 20, marginLeft: 2,}}>
                                                {childItem}
                                            </Text>
                                            :
                                            <Text
                                                style={{color: 'black', fontSize: 20, marginLeft: 2,}}>
                                                {childItem},
                                            </Text>
                                        }


                                    </TouchableOpacity>
                                )
                            })}

                        </View>

                        </Body>
                    </CardItem>


                </Card>
            )
        }

        onSwipeUp(gestureState) {
            this.setState({myText: 'You swiped up!'});
        }

        onSwipeDown(gestureState) {
            this.setState({myText: 'You swiped down!'});
        }

        onSwipeLeft(gestureState) {
            this.setState({myText: 'You swiped left!'});
        }

        onSwipeRight(gestureState) {
            this.setState({myText: 'You swiped right!'});
        }

        onSwipe(gestureName, gestureState) {
            const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
            this.setState({gestureName: gestureName});
            switch (gestureName) {
                case SWIPE_UP:
                    this.setState({backgroundColor: 'red'});
                    break;
                case SWIPE_DOWN:
                    this.setState({backgroundColor: 'green'});
                    break;
                case SWIPE_LEFT:
                    this.setState({backgroundColor: 'blue'});
                    break;
                case SWIPE_RIGHT:
                    this.setState({backgroundColor: 'yellow'});
                    break;
            }
        }


        render() {

            const config = {
                velocityThreshold: 0.3,
                directionalOffsetThreshold: 80
            };
            if (!this.state.isReady) {
                return;
            }
            return (

                <FlingGestureHandler
                    direction={Directions.LEFT}
                    onHandlerStateChange={({nativeEvent}) => {
                        console.log("moveRight-->", nativeEvent);
                        if (nativeEvent.oldState === State.ACTIVE) {

                            if (this.state.currentTabIndex<this.cates.length-1){
                                this.changeTab(this.cates[this.state.currentTabIndex + 1], this.state.currentTabIndex + 1)
                            }

                        }
                    }}>
                    <FlingGestureHandler
                        direction={Directions.RIGHT}
                        onHandlerStateChange={({nativeEvent}) => {
                            if (nativeEvent.oldState === State.ACTIVE) {

                                if (this.state.currentTabIndex!==0){
                                    this.changeTab(this.cates[this.state.currentTabIndex - 1], this.state.currentTabIndex - 1)
                                }
                            }
                        }}>

                        <Container style={{backgroundColor: '#e3e3e3'}}>
                            <Header style={{backgroundColor: '#4e0db4'}}>
                                <Body>
                                <Title style={{marginLeft: 10, marginBottom: 0}}>
                                    {this.state.cate}
                                </Title>
                                </Body>
                            </Header>

                            <View style={{position: 'absolute', top: 50, height: 55}}>
                                {this.renderTabTab()}
                            </View>

                            {this.state.loading && <View style={{marginTop: 100}}>
                                <ActivityIndicator color={'red'}/>
                            </View>}

                            {/*sdlfksdlkfasldkflskdflksadlfklsdf*/}
                            {/*sdlfksdlkfasldkflskdflksadlfklsdf*/}
                            {/*sdlfksdlkfasldkflskdflksadlfklsdf*/}


                            <FlatList
                                style={{marginTop: 50, marginBottom: 50}}
                                data={GlobalStore.japanList}
                                renderItem={this._renderItem}
                                onEndReached={this.state.isMoreData && this._getMoreDatas}
                                onEndReachedThreshold={0.01}
                                initialNumToRender={30}
                                keyExtractor={(item, index) => index.toString()}
                                ListFooterComponent={() => {
                                    if (this.state.bottomLoading) {
                                        return (
                                            <View style={{marginBottom: 30}}>
                                                <ActivityIndicator color={'blue'}/>
                                            </View>
                                        )
                                    } else {
                                        return null;
                                    }
                                }}
                            />


                            {/*sdlfksdlkflsdkflsdklfksdlfklfdsk*/}
                            {/* <ScrollView
                        style={{marginBottom: 55, marginTop: 55}}
                    >
                        {this.state.results.map((item, index) => {


                        })}
                    </ScrollView>*/}
                            <View style={{
                                alignSelf: 'center',
                                alignItem: 'center',
                                justifyContent: "center",
                                position: 'absolute',
                                bottom: 0
                            }}>
                                {renderBanner()}
                            </View>
                        </Container>

                    </FlingGestureHandler>
                </FlingGestureHandler>


            );
        }
    }
)


