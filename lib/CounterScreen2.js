// @flow

import React, {Component} from 'react';
import {Container, Text, Card, Header, Body, Button, Title, CardItem} from 'native-base';
import GlobalStore from './GlobalStore';
import {observer} from 'mobx-react';
import {View, ActivityIndicator} from 'react-native';


/**
 * CounterScreen2
 */
export default observer(
    class CounterScreen2 extends Component {
        constructor() {
            super();
            this.state = {
                isReady: false
            }
        }

        async componentWillMount() {
            GlobalStore.incrementDouble();
            this.setState({isReady: true});
        }

        render() {
            if (!this.state.isReady) {
                return;
            }
            return (
                <Container>
                    <View>
                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 30,
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center'
                        }}>
                            {GlobalStore.counter}
                        </Text>
                    </View>

                    <Button primary block onPress={() => GlobalStore.increment()}>
                        <Text>Increment</Text>
                    </Button>
                    <Button primary block onPress={() => GlobalStore.decrement()}>
                        <Text>Decrement</Text>
                    </Button>

                    <Button warning block onPress={() => GlobalStore.incrementDouble()}>
                        <Text>INcrement Double</Text>
                    </Button>
                    <Button style={{backgroundColor: 'green'}} block onPress={() => GlobalStore.incrementDouble()}>
                        <Text>INcrement Double</Text>
                    </Button>
                    {GlobalStore.loading && <ActivityIndicator/>}

                    <Button style={{backgroundColor: 'green'}} block onPress={() =>{

                        GlobalStore.toggleLoading();
                    }}>
                        <Text>Toggle Laoding</Text>
                    </Button>

                    <View style={{height: 50}}/>
                    <Button style={{backgroundColor: 'red'}} onPress={() => {

                        this.props.navigation.push('CounterScreen2')
                    }}>
                        <Text>CounterScreen2</Text>
                    </Button>
                    <View style={{height:15}}/>
                    <Button style={{backgroundColor: 'maroon'}} onPress={() => {

                        this.props.navigation.push('CounterScreen3')
                    }}>
                        <Text>CounterScreen3</Text>
                    </Button>
                </Container>
            );
        }
    }
)
