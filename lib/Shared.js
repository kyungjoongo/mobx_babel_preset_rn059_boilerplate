import {AdMobBanner, AdMobInterstitial,} from 'react-native-admob'
import {View} from "react-native";
import {Container} from "native-base";
import React from "react";
import Toast from "react-native-root-toast";

export const getRandomNum = () => {
    return 'favorite_' + Math.random().toString(36).substr(2, 9);
};


export const showAdmobInit = () => {
    AdMobInterstitial.setAdUnitID('ca-app-pub-6826082357124500/1071396215');
    AdMobInterstitial.requestAd().then(() => {
            AdMobInterstitial.showAd().then(() => {
            })
        }
    );
}


export const renderBanner = () => {
    return (
        <AdMobBanner
            adSize="banner"
            adUnitID="ca-app-pub-6826082357124500/7525123928"
            testDevices={[AdMobBanner.simulatorId]}
        />
    )
}

export const showToast = (text) =>{
    Toast.show(text, {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
    });
}
